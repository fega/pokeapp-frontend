import Vue from 'vue';
import Vuex from 'vuex';
import API from '@/api';
import localstore from 'store';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    title: 'PokeApp',
    myPokemons: [],
    pokemons: [
    ],
    pokemonNames: ['bulbasaur', 'ivysaur', 'venusaur', 'charmander', 'charmeleon', 'charizard', 'squirtle', 'wartortle', 'blastoise', 'caterpie', 'metapod', 'butterfree', 'weedle', 'kakuna', 'beedrill', 'pidgey', 'pidgeotto', 'pidgeot', 'rattata', 'raticate', 'spearow', 'fearow', 'ekans', 'arbok', 'pikachu', 'raichu', 'sandshrew', 'sandslash', 'nidoran♀', 'nidorina', 'nidoqueen', 'nidoran♂', 'nidorino', 'nidoking', 'clefairy', 'clefable', 'vulpix', 'ninetales', 'jigglypuff', 'wigglytuff', 'zubat', 'golbat', 'oddish', 'gloom', 'vileplume', 'paras', 'parasect', 'venonat', 'venomoth', 'diglett', 'dugtrio', 'meowth', 'persian', 'psyduck', 'golduck', 'mankey', 'primeape', 'growlithe', 'arcanine', 'poliwag', 'poliwhirl', 'poliwrath', 'abra', 'kadabra', 'alakazam', 'machop', 'machoke', 'machamp', 'bellsprout', 'weepinbell', 'victreebel', 'tentacool', 'tentacruel', 'geodude', 'graveler', 'golem', 'ponyta', 'rapidash', 'slowpoke', 'slowbro', 'magnemite', 'magneton', "farfetch\'d", 'doduo', 'dodrio', 'seel', 'dewgong', 'grimer', 'muk', 'shellder', 'cloyster', 'gastly', 'haunter', 'gengar', 'onix', 'drowzee', 'hypno', 'krabby', 'kingler', 'voltorb', 'electrode', 'exeggcute', 'exeggutor', 'cubone', 'marowak', 'hitmonlee', 'hitmonchan', 'lickitung', 'koffing', 'weezing', 'rhyhorn', 'rhydon', 'chansey', 'tangela', 'kangaskhan', 'horsea', 'seadra', 'goldeen', 'seaking', 'staryu', 'starmie', 'mr. mime', 'scyther', 'jynx', 'electabuzz', 'magmar', 'pinsir', 'tauros', 'magikarp', 'gyarados', 'lapras', 'ditto', 'eevee', 'vaporeon', 'jolteon', 'flareon', 'porygon', 'omanyte', 'omastar', 'kabuto', 'kabutops', 'aerodactyl', 'snorlax', 'articuno', 'zapdos', 'moltres', 'dratini', 'dragonair', 'dragonite', 'mewtwo', 'mew'],
    user: null,
    jwt: null,
    barColor: 'primary',
    name: undefined,
    email: undefined,
  },
  mutations: {
    SET_POKEMONS(state, pokemons) {
      state.pokemons = pokemons || [];
    },
    SET_JWT(state, jwt) {
      state.jwt = jwt;
    },
    SET_USER(state, user) {
      state.user = user;
    },
    SET_TRAINEE_POKEMONS(state, pokemons) {
      state.myPokemons = pokemons;
    },
  },
  actions: {
    async SET_POKEMONS({ commit, state }) {
      if (!state.pokemons.length) {
        const pokemons = await API.GET_POKEMONS();
        commit('SET_POKEMONS', pokemons);
      }
    },
    async SET_TRAINEE_POKEMONS({ commit, state }) {
      if (!state.pokemons.length) {
        const pokemons = await API.GET_TRAINEE_POKEMONS();
        commit('SET_TRAINEE_POKEMONS', pokemons);
      }
    },
    GET_JWT_FROM_LOCALSTORAGE({ commit }) {
      const jwt = localStorage.get('jwt');
      commit('SET_JWT', jwt);
    },
    async LOGIN({ commit, state }, { email, password }) {
      if (!state.user) { // this should have a better handling
        const { jwt, ...user } = await API.LOGIN(email, password);
        localstore.set('jwt', jwt);
        localstore.set('id', user.id);
        commit('SET_JWT', jwt);
        commit('SET_USER', user);
      }
    },
    async GET_USER({ commit }) {
      const jwt = localstore.get('jwt');
      if (!jwt) return commit('SET_USER', null);

      const user = await API.GET_PROFILE().catch(() => {
        localstore.clearAll();
      });
      commit('SET_USER', user);
      return commit('SET_JWT', jwt);
    },
    async LOGOUT({ commit }) {
      localstore.set('jwt', null);
      localstore.set('id', null);
      commit('SET_JWT', null);
      commit('SET_USER', null);
    },
    async ADD_TRAINEE_POKEMON(ctx, { name, nickname, level }) {
      await API.POST_TRAINEE_POKEMON(name, nickname, level);
    },
  },
});
