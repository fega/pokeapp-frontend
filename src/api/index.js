import axios from 'axios';
import localstore from 'store';

const BASE_URL = 'https://poke-app-dex.herokuapp.com/api/v1';
class API {
  async LOGIN(email, password) {
    const r = await axios.post(`${BASE_URL}/login`, {
      password,
      email,
    });
    return r.data;
  }
  async GET_POKEMONS() {
    return (await axios.get(`${BASE_URL}/pokemons?limit=160`)).data;
  }
  async GET_TRAINEE_POKEMONS() {
    const jwt = localstore.get('jwt');
    const r = await axios({
      method: 'GET',
      url: `${BASE_URL}/users/me/pokemons?limit=100`,
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    return r.data;
  }
  async POST_TRAINEE_POKEMON(name, nickname, level) {
    const jwt = localstore.get('jwt');
    const r = await axios({
      method: 'POST',
      url: `${BASE_URL}/users/me/pokemons`,
      data: {
        name, nickname, level,
      },
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    return r.data;
  }
  async GET_PROFILE() {
    const jwt = localstore.get('jwt');
    const id = localstore.get('id');
    const r = await axios({
      method: 'GET',
      url: `${BASE_URL}/users/${id}`,
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    return r.data;
  }
}
export default new API();
