import Vue from 'vue';
import Store from '@/store';
import Router from 'vue-router';
import List from './views/List.vue';
import Login from './views/Login.vue';
import Details from './views/Details.vue';
import Profile from './views/Profile.vue';
import Page404 from './views/Page404.vue';
import store from 'store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'about',
      component: Login,
    },
    {
      path: '/details',
      name: 'details',
      component: Details,
    },
    {
      path: '/pokemons/:pokemonName',
      name: 'details',
      props: true,
      component: Details,
    },
    {
      path: '/pokemons',
      name: 'Pokemons',
      component: List,
    },
    {
      path: '/trainer',
      name: 'Trainer',
      component: Profile,
    },
    {
      path: '*',
      name: 'Not found',
      component: Page404,
    },
  ],
});
export default router;
router.beforeEach(async (to, from, next) => {
  if (Store.state.user === null && to.path !== '/') {
    next('/');
  } else if (store.get('jwt') && Store.state.user === null) {
    await Store.dispatch('GET_USER');
    next('/trainer');
  } else if (Store.state.user && to.path === '/') {
    next('/trainer');
  } else next();
});

